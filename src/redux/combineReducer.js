import { combineReducers } from "redux";
import productSlice from "../modules/product/core/reducer"
import categorySlice from "../modules/category/core/reducer"

export const rootReducers = combineReducers({
    product: productSlice,
    category: categorySlice,
})