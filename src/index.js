import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {AuthProvider} from "./modules/auth/Components/Auth";
import {AuthInit} from "./modules/auth/Components/AuthInit";
import {setUpAxios} from "./modules/auth/Components/AuthHelper";
import 'mdb-react-ui-kit/dist/css/mdb.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import {Provider} from "react-redux";
import {store} from "./redux/store";

const root = ReactDOM.createRoot(document.getElementById('root'));
setUpAxios()
root.render(
  <React.StrictMode>
    <Provider store={store}>
        <AuthProvider>
            <AuthInit>
                <App />
            </AuthInit>
        </AuthProvider>
    </Provider>
  </React.StrictMode>
);
