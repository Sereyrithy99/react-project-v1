import { useDispatch } from "react-redux"

import { setProduct, setTotal } from "./reducer";
import {fetchProduct, fetchProductByCategory, fetchSearchProduct} from "./request";


const useProduct = () => {
    const dispatch = useDispatch();
    const getProducts = (limit) => {
        fetchProduct(limit).then((res) => {
            dispatch(setProduct(res.data.products));
            dispatch(setTotal(res.data.total));
        });
    }
    const getProductsByCategory = (limit, active) => {
        fetchProductByCategory(limit, active).then((res) => {
            dispatch(setProduct(res.data.products));
            dispatch(setTotal(res.data.total));
        })
    }
    const searchProducts = (text) => {
        fetchSearchProduct(text).then((res) => {
            dispatch(setProduct(res.data.products));
            dispatch(setProduct(res.data.total));
        })
    }

    return {getProducts, getProductsByCategory, searchProducts}
}

export {useProduct}