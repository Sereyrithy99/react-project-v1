import { createSlice } from "@reduxjs/toolkit";

const productSlice = createSlice({
    name: "product",
    initialState: {
        products: [],
        total: 0,
    },
    reducers: {
        setProduct(state, action) {
            state.products = action.payload;
        },
        setTotal(state, action) {
            state.total = action.payload;
        }
    }
})

export const {setProduct, setTotal} = productSlice.actions;

export default productSlice.reducer;