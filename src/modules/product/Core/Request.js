import axios from "axios";

const fetchProduct = (limit = 10, skip =0) => {
  return axios.get("/products", {
    params: {limit, skip}
  })
}

const fetchProductById = (id) => {
  return axios.get(`/products/${id}`)
}

const fetchSearchProduct = (params) => {
  return axios.get("/products/search", {
    params: params
  });
}


const fetchProductByCategory = (limit = 10, path = "") => {
  return axios.get(`/products/category/${path}`, {
    params: {limit}
  })
}

export {fetchProduct, fetchProductById, fetchSearchProduct,  fetchProductByCategory}