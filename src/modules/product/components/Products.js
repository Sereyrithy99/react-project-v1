import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import PaginationBasic from "./Pagination";
import { MDBInputGroup, MDBInput, MDBIcon, MDBBtn } from "mdb-react-ui-kit";
import { useProduct } from "../core/action";
import {useSelector} from "react-redux";
import useCategory from "../../category/core/action";
import ListCategories from "../../category/components/Categories";

export default function ListProducts() {

  const [active, setActive] = useState("All");
  const [searchText, setSearchText] = useState("");
  const navigate = useNavigate();
  const limit = 8;
  const {getProducts, getProductsByCategory, searchProducts} = useProduct();
  const {getCategories} = useCategory();
  const {products, total} = useSelector(state => state.product)

  useEffect(() => {
    getProducts(limit);
    getCategories();
  }, [])

  useEffect(() => {
    active === "All" ?
    getProducts(limit) : getProductsByCategory(limit, active)
  }, [active]);

  const onSearch = () => {
    searchProducts({q: searchText})
  }
  
  return (
    <div>
      {/*Search Product*/}
      <div className="container-sm col-xl-3">
        <div className="m-5 d-flex justify-center align-items-center">
          <MDBInputGroup>
            <MDBInput
              onChange={(e) => setSearchText(e.target.value)}
              label="Search"
            />
            <MDBBtn rippleColor="dark" onClick={onSearch}>
              <MDBIcon icon="search"/>
            </MDBBtn>
          </MDBInputGroup>
        </div>
      </div>

      {/*Select Product By Category*/}

      <ListCategories active={active} setActive={setActive}/>

      {/*List Product and Select Product By ID*/}
      <div className="d-flex flex-wrap justify-content-center font-medium cursor-pointer">
        {products.length === 0 && <h4 className="text-dark">Not Found</h4>}
        {products.map((product, index) => {
          return (
            <div
              onClick={() => navigate(`/product/${product.id}`)}
              className="mx-2 border rounded-2 my-2"
              key={index}
            >
              <div className="divide-y divide-gray-300/50 cursor-pointer">
                <img
                  style={{ height: "200px", width: "300px" }}
                  src={product.thumbnail}
                  alt="__"
                />
                <div>
                  <div className="d-flex justify-content-between px-2 mt-3">
                    <p>{product.title}</p>
                    <p className=" text-danger">${product.price}</p>
                  </div>
                  <p className="px-2 d-flex justify-content-between">
                    Discount:{" "}
                    <span className="text-danger">
                      %{product.discountPercentage}
                    </span>
                  </p>
                  <p className="px-2">Brand: {product.brand}</p>
                  <p className="px-2">Tags: {product.category}</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>

      <div className=" d-flex justify-content-center align-items-center">
        <PaginationBasic onChangePage={(page) => {
          getProducts(limit, (page - 1) * limit);
        }} total={total / limit}/>
      </div>
    </div>
  );
}
