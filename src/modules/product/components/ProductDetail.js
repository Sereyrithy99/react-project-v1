import { Route, Routes, useParams} from "react-router-dom"

// nested routes
import Offers from "../../pages/Offers"
import {useEffect, useState} from "react";
import { MDBSpinner } from "mdb-react-ui-kit";
import {fetchProductById} from "../core/request";

export default function ProductDetails() {

    const {id} = useParams();
    const [product, setProduct] = useState({})

    useEffect(() => {
        fetchProductById(id).then((res) => {
            setProduct((res.data))
        })
    }, [id]);

    if (!product.id) {
        return (
            <div className="text-center mt-5 ">
                <MDBSpinner color='primary'>
                    <span className='visually-hidden'>Loading...</span>
                </MDBSpinner>
            </div>
        )
    }

    return (
        <div className="content">
            <div className="product">
                <div className="image">
                    <img src={product.thumbnail} alt="__"/>
                </div>
                <div className="details text-dark">
                    <h5 className="text-capitalize">{product.title}</h5>
                    <p className="square border border-2 d-inline-block rounded px-2 py-1 text-capitalize">{product.category}</p>
                    <h5 className="text-danger">${product.price}</h5>
                    <p>{product.description}</p>
                    <p>Rating: <span className="text-primary">{product.rating}</span></p>
                    <p>In Stock: <span className="text-primary">{product.stock}</span> </p>
                </div>
            </div>

            <Routes>
                <Route path="offers" element={<Offers />} />
            </Routes>
        </div>
    )
}
