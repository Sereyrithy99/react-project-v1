import { useState } from 'react'
import {useAuth} from "./Auth";
import {login} from "../Core/request";
import {
  MDBBtn,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBInput,
}
from 'mdb-react-ui-kit';

const Login = () => {
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");
  const [error, setError] = useState("");
  const {saveAuth} = useAuth();

  const onLogin = (event) => {
    event.preventDefault();
    login(username,pass).then((response) => {
      setError("");
      saveAuth(response.data.token)
    }).catch((error) => {
      setError(error.response.data.message);
    })
  }

  return (
    <MDBContainer fluid>

    <MDBRow className=' d-flex justify-content-center align-items-center h-200'>
      <MDBCol col='12'>

        <MDBCard className='bg-white my-5 mx-auto' style={{borderRadius: '1rem', maxWidth: '400px'}}>
          <MDBCardBody className='p-5 w-100 d-flex flex-column'>

            <h2 className="fw-bold mb-2 text-center text-dark">Login</h2>
            <p className="text-white-50 mb-3">Please enter your login and password!</p>

            <MDBInput 
            wrapperClass='mb-4 w-100' 
            label='Username' 
            id='formControlLg' 
            type='email' 
            size="md"
            onChange={(e) => setUsername(e.target.value)} 
            />
            <MDBInput 
            wrapperClass='mb-4 w-100' 
            label='Password'
            id='formControlLg' 
            type='password' 
            size="md"
            onChange={(e) => setPass(e.target.value)}
            />

            <div className="text-center text-danger">{error}</div>
            <MDBBtn size='md' onClick={onLogin}>
              Login
            </MDBBtn>

          </MDBCardBody>
        </MDBCard>

      </MDBCol>
    </MDBRow>

  </MDBContainer>
  )
}

export default Login
