import {useDispatch} from "react-redux";
import {fetchCategories} from "./request";
import {setCategories} from "./reducer";

const useCategory = () => {
    const dispatch = useDispatch();
    const getCategories = (limit) => {
        fetchCategories(limit).then((res) => {
            dispatch(setCategories(res.data))
        })
    }

    return {getCategories}
}

export default useCategory