import axios from "axios";
const fetchCategories = () => {
    return axios.get("/products/categories")
}

export {fetchCategories}