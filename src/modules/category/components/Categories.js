import {useSelector} from "react-redux";
import {Badge} from "react-bootstrap";

const ListCategories = ({active, setActive}) => {
    const {categories} = useSelector(state => state.category)

    return <div className=" d-flex flex-wrap justify-content-center align-content-center">
        <Badge
            onClick={() => setActive("All")}
            className="px-2 py-2 fs-6 mx-2 my-2 cursor-pointer"
            bg={active === "All" ? "primary" : "dark"}
        >
            All
        </Badge>
        {categories.map((item, index) => (
            <Badge
                onClick={() => setActive(item)}
                key={index}
                className="px-2 py-2 fs-6 mx-2 my-2 cursor-pointer"
                bg={active === item ? "primary" : "dark"}
            >
                {item}
            </Badge>
        ))}
    </div>
}

export default ListCategories;