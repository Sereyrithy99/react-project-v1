import Header from "./navbar/Navbar";
import {Outlet} from "react-router-dom";

export default function Layout() {
  return (
    <>
      <Header/>
      <Outlet/>
    </>
  )
}
