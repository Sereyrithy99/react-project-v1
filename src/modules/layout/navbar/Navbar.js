import {Link} from "react-router-dom";
import React, { useState } from 'react';
import {useAuth} from "../../auth/Components/Auth";
import {
  MDBNavbar,
  MDBContainer,
  MDBNavbarBrand,
  MDBIcon,
  MDBNavbarNav,
  MDBNavbarItem,
  MDBNavbarLink,
  MDBNavbarToggler,
  MDBCollapse
} from 'mdb-react-ui-kit';
import { Button } from "react-bootstrap";

const Header = () => {
  const [openNavText, setOpenNavText] = useState(false);
  const {logout} = useAuth();
  return (
    <MDBNavbar expand='lg' light bgColor='light'>
    <MDBContainer fluid>
      <MDBNavbarBrand>
      <Link to="/">The Store</Link>
      </MDBNavbarBrand>
      <MDBNavbarToggler
        type='button'
        data-target='#navbarText'
        aria-controls='navbarText'
        aria-expanded='false'
        aria-label='Toggle navigation'
        onClick={() => setOpenNavText(!openNavText)}
      >
        <MDBIcon icon='bars' fas />
      </MDBNavbarToggler>
      <MDBCollapse navbar open={openNavText}>
        <MDBNavbarNav className='mr-auto mb-2 mb-lg-0'>
          <MDBNavbarItem>
            <MDBNavbarLink>
              <Link to="/about">About</Link>
            </MDBNavbarLink>
          </MDBNavbarItem>
          <MDBNavbarItem>
            <MDBNavbarLink>
            <Link to="/products">Product</Link>
            </MDBNavbarLink>
          </MDBNavbarItem>
        </MDBNavbarNav>
        <Button onClick={logout} className="px-2">Logout</Button>
      </MDBCollapse>
    </MDBContainer>
  </MDBNavbar>
  );
};

export default Header;
