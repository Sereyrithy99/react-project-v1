import React from 'react'
import ListProducts from '../product/components/Products'

export default function HomePage(props) {
  return (
    <div className="text-center">
      <ListProducts/>
    </div>
  )
}
