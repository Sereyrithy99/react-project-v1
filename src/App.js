import {BrowserRouter, Navigate, Route, Routes} from 'react-router-dom';
// pages
import About from './modules/pages/About';
import Login from './modules/auth/Components/Login';
import Layout from "./modules/layout";
import ErrorPage from "./modules/error/ErrorPage"
import {useAuth} from "./modules/auth/Components/Auth";
import HomePage from './modules/home';
import ProductDetails from "./modules/product/components/ProductDetail";
function App() {
  const {auth} = useAuth();

  return (
      <BrowserRouter>
        <Routes>
          {auth ?
            <Route path='/' element={<Layout/>}>
              <Route index element={<HomePage/>} />
              <Route path='/about/*' element={<About/>} />
              <Route path='/product/:id/*' element={<ProductDetails/>} />
              <Route path='*' element={<Navigate to='/' />} />
            </Route>
            :
            <>
              <Route path='auth/login' element={<Login/>} />
              <Route path="*" element={<Navigate to='/auth/login'/> }/>
            </>
          }
          <Route path='error' element={<ErrorPage/>} />
        </Routes>
      </BrowserRouter>
  );
}

export default App;
